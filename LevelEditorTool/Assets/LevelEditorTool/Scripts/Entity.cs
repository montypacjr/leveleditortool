﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LMGTool
{
	[System.Serializable]
	public class Entity
	{
		public GameObject prefab;
		public GameObject[] pool;
		public Color color;

		public bool random;

		[SerializeField]
		private bool poolCollapse;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace LMGTool
{
	[CustomPropertyDrawer (typeof(MiscObject))]
	public class MiscObjectDrawer : PropertyDrawer
	{
		LMGEditorWindow myWindow;

		SerializedProperty prefab;
		SerializedProperty color;
		SerializedProperty rotation;
		SerializedProperty scale;
		SerializedProperty minRotation;
		SerializedProperty maxRotation;
		SerializedProperty minScale;
		SerializedProperty maxScale;

		SerializedProperty rotCollapse;
		SerializedProperty scaleCollapse;
		SerializedProperty doubleOption;

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
//			if (myWindow == null)
//				myWindow = (LMGEditorWindow)EditorWindow.GetWindow (typeof(LMGEditorWindow));
		
			SetProperties (property);

			int oldIndentLevel = EditorGUI.indentLevel;
			label = EditorGUI.BeginProperty (position, label, property);
			Rect contentPosition = EditorGUI.PrefixLabel (position, label);
			contentPosition.height = 16f;
//			if (position.height > 16f) {
//				position.height = 16f;
//				EditorGUI.indentLevel += 1;
//				contentPosition = EditorGUI.IndentedRect (position);
//				contentPosition.y += 20f;
//			}
			contentPosition.width *= 0.45f;
			EditorGUI.indentLevel = 0;
			EditorGUI.PropertyField (contentPosition, prefab, GUIContent.none);
			contentPosition.x += contentPosition.width;
			contentPosition.width /= 2f;
			EditorGUIUtility.labelWidth = 12f;
			EditorGUI.BeginChangeCheck ();
			EditorGUI.PropertyField (contentPosition, color, new GUIContent ("C"));
			if (EditorGUI.EndChangeCheck () && myWindow != null) {
				if (prefab.objectReferenceValue != null) {
					myWindow.SetColor (color.colorValue, (prefab.objectReferenceValue as GameObject).name);
				} else
					Debug.LogWarning ("No prefab has been asigned, check you references");
				if (color != null) {
					Color aux = color.colorValue;
					aux.a = 1.0f;
					color.colorValue = aux;
				}
			}
			contentPosition.x += contentPosition.width;
			contentPosition.width /= 2f;
			EditorGUI.PropertyField (contentPosition, rotation, new GUIContent ("R", "Random rotation between values?"));
			contentPosition.x += contentPosition.width;
			EditorGUI.PropertyField (contentPosition, scale, new GUIContent ("S", "Random scale between values?"));
			contentPosition.x += contentPosition.width;
			contentPosition.width /= 1.1f;
			if (GUI.Button (contentPosition, new GUIContent ("+", "Set color to the window brush"), EditorStyles.miniButton)) {
				// set color left button editor window
				if (myWindow == null)
					myWindow = (LMGEditorWindow)EditorWindow.GetWindow (typeof(LMGEditorWindow));
				if (prefab.objectReferenceValue != null) {
					myWindow.SetColor (color.colorValue, (prefab.objectReferenceValue as GameObject).name);
				} else
					Debug.LogWarning ("No prefab has been asigned, check you references");
			}
			//añadir un boton de seteo de color en la ventana

			if (rotation.boolValue && scale.boolValue)
				doubleOption.boolValue = true;
			else
				doubleOption.boolValue = false;

			EditorGUI.indentLevel = oldIndentLevel + 1;
			Rect singleOptionRect = new Rect (position.x, position.y + 18f, position.width, 18f + 5f);
			using (var group = new EditorGUILayout.FadeGroupScope (Convert.ToSingle (rotation.boolValue))) {
				if (group.visible) {
					rotCollapse.boolValue = EditorGUI.Foldout (singleOptionRect, rotCollapse.boolValue, new GUIContent ("Random Rotation", "Random values between 2 vectors."), true);
					EditorGUI.indentLevel++;
					Rect firstProp = new Rect (position.x, singleOptionRect.y + 18f, position.width, singleOptionRect.height * 4f);
					Rect secondProp = new Rect (position.x, firstProp.y + 18f * 2f, position.width, firstProp.height);

					if (rotCollapse.boolValue) {

						EditorGUI.PropertyField (firstProp, minRotation, new GUIContent ("Minimun", "Minimun rotation factor"));

						EditorGUI.PropertyField (secondProp, maxRotation, new GUIContent ("Maximun", "Maximun rotation factor"));

					}
					EditorGUI.indentLevel--;
				}
			}
				
			Rect doubleOptionRect;
			if (rotCollapse.boolValue)
				doubleOptionRect = new Rect (position.x, position.y + 18f * 6f + 5f, position.width, 18f + 5f);
			else
				doubleOptionRect = new Rect (position.x, singleOptionRect.y + 18f + 5f, position.width, 18f + 5f);
			using (var group = new EditorGUILayout.FadeGroupScope (Convert.ToSingle (scale.boolValue))) {
				if (group.visible) {
					Rect firstProp;
					Rect secondProp;
					if (doubleOption.boolValue) {
						scaleCollapse.boolValue = EditorGUI.Foldout (doubleOptionRect, scaleCollapse.boolValue, new GUIContent ("Random Scale", "Random values between 2 vectors."), true);
						firstProp = new Rect (position.x, doubleOptionRect.y + 18f, position.width, doubleOptionRect.height * 4f);
						secondProp = new Rect (position.x, firstProp.y + 18f * 2f, position.width, firstProp.height);
					} else {
						scaleCollapse.boolValue = EditorGUI.Foldout (singleOptionRect, scaleCollapse.boolValue, new GUIContent ("Random Scale", "Random values between 2 vectors."), true);
						firstProp = new Rect (position.x, singleOptionRect.y + 18f, position.width, singleOptionRect.height * 4f);
						secondProp = new Rect (position.x, firstProp.y + 18f * 2f, position.width, firstProp.height);
					}

					EditorGUI.indentLevel++;
					if (scaleCollapse.boolValue) {

						EditorGUI.PropertyField (firstProp, minScale, new GUIContent ("Minimun", "Minimun scale factor"));

						EditorGUI.PropertyField (secondProp, maxScale, new GUIContent ("Maximun", "Maximun scale factor"));

					}
					EditorGUI.indentLevel--;
				}
			}

			EditorGUI.EndProperty ();

			EditorGUI.indentLevel = oldIndentLevel;
		}

		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			SetProperties (property);
			float height = 0f;

			if (!rotation.boolValue && !scale.boolValue)
				height = label != GUIContent.none && Screen.width < 333 ? (16f + 18f) : 16f;
			else if (rotation.boolValue && scale.boolValue) {
				if (!rotCollapse.boolValue && !scaleCollapse.boolValue) {
					height = 16f + (18f * 2f) + 10f;
				} else if (!rotCollapse.boolValue || !scaleCollapse.boolValue)
					height = 16f + (18f * 6f) + 10f;
				else
					height = 16f + (18f * 10f) + 10f;
			} else if (rotation.boolValue || scale.boolValue) {
				if (rotation.boolValue && !rotCollapse.boolValue) {
					height = 16f + 18f + 5f;
				} else if (scale.boolValue && !scaleCollapse.boolValue)
					height = 16f + 18f + 5f;
				else
					height = 16f + (18f * 5f) + 5f;
			}
			return height;
		}

		void SetProperties (SerializedProperty property)
		{
			rotCollapse = property.FindPropertyRelative ("rotCollapse");
			scaleCollapse = property.FindPropertyRelative ("scaleCollapse");
			doubleOption = property.FindPropertyRelative ("doubleOption");
			prefab = property.FindPropertyRelative ("prefab");
			color = property.FindPropertyRelative ("color");
			rotation = property.FindPropertyRelative ("rotation");
			scale = property.FindPropertyRelative ("scale");
			minRotation = property.FindPropertyRelative ("minRotation");
			maxRotation = property.FindPropertyRelative ("maxRotation");
			minScale = property.FindPropertyRelative ("minScale");
			maxScale = property.FindPropertyRelative ("maxScale");
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LMGTool;

[CustomPropertyDrawer (typeof(Entity))]
public class EntityDrawer : PropertyDrawer
{
	LMGEditorWindow myWindow;
	SerializedProperty color;
	SerializedProperty prefab;
	SerializedProperty random;
	SerializedProperty pool;
	SerializedProperty poolCollapse;

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		//			if (myWindow == null)
		//				myWindow = (LMGEditorWindow)EditorWindow.GetWindow (typeof(LMGEditorWindow));

		SetProperties (property);

		int oldIndentLevel = EditorGUI.indentLevel;
		label = EditorGUI.BeginProperty (position, label, property);
		Rect contentPosition = EditorGUI.PrefixLabel (position, label);
		Rect prefixLabel = EditorGUI.PrefixLabel (position, label);
		contentPosition.height = 16f;
//		if (position.height > 16f) {
//			position.height = 16f;
//			EditorGUI.indentLevel += 1;
//			contentPosition = EditorGUI.IndentedRect (position);
//			contentPosition.y += 18f;
//		}
		contentPosition.width *= 0.55f;
		EditorGUI.indentLevel = 0;
		//provisional
		if (!random.boolValue)
			EditorGUI.PropertyField (contentPosition, prefab, GUIContent.none);
		else
			poolCollapse.boolValue = EditorGUI.Foldout (contentPosition, poolCollapse.boolValue, new GUIContent ("Object pool"));
		contentPosition.x += contentPosition.width;
		contentPosition.width /= 2.5f;
		EditorGUIUtility.labelWidth = 14f;
		EditorGUI.BeginChangeCheck ();
		EditorGUI.PropertyField (contentPosition, color, new GUIContent ("C"));
		if (EditorGUI.EndChangeCheck () && myWindow != null) {
			if (prefab.objectReferenceValue != null) {
				myWindow.SetColor (color.colorValue, (prefab.objectReferenceValue as GameObject).name);
			} else
				Debug.LogWarning ("No prefab has been asigned, check you references");
		}
		contentPosition.x += contentPosition.width + 5f;
		contentPosition.width /= 2.5f;
		if (GUI.Button (contentPosition, new GUIContent ("+", "Set color to the window brush"), EditorStyles.miniButton)) {
			// set color left button editor window
			if (myWindow == null)
				myWindow = (LMGEditorWindow)EditorWindow.GetWindow (typeof(LMGEditorWindow));
			if (prefab.objectReferenceValue != null) {
				myWindow.SetColor (color.colorValue, (prefab.objectReferenceValue as GameObject).name);
			} else
				Debug.LogWarning ("No prefab has been asigned, check you references");
		}
		EditorGUIUtility.labelWidth = 14f;
		contentPosition.x += contentPosition.width + 5f;
		EditorGUI.PropertyField (contentPosition, random, new GUIContent ("R", "Random object from a pool?"));
		// si elige que el objeto sea random, poner el array 
		int poolSize = pool.arraySize;

		Rect sizeRect = new Rect (prefixLabel.x + 20f, prefixLabel.y + 18f, prefixLabel.width - 20f, 16f);
		if (random.boolValue && poolCollapse.boolValue) {
			EditorGUI.PropertyField (sizeRect, pool.FindPropertyRelative ("Array.size"), GUIContent.none);
			for (int i = 0; i < poolSize; i++) {
				Rect objectRect = new Rect (sizeRect.x, sizeRect.y + (18f * (i + 1)), sizeRect.width, 16f);
				EditorGUI.PropertyField (objectRect, pool.GetArrayElementAtIndex (i), GUIContent.none);
			}
		}

		EditorGUI.EndProperty ();
		EditorGUI.indentLevel = oldIndentLevel;
	}

	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		SetProperties (property);
		float height;
		if (random.boolValue) {
			float multiplier = pool.arraySize + 1;
			if (poolCollapse.boolValue)
				height = 16f + (18f * multiplier) + 5;
			else
				height = 16f;
		} else
			height = label != GUIContent.none && Screen.width < 333 ? (16f + 18f) : 16f;

		return height;
	}

	void SetProperties (SerializedProperty property)
	{
		color = property.FindPropertyRelative ("color");
		prefab = property.FindPropertyRelative ("prefab");
		random = property.FindPropertyRelative ("random");
		pool = property.FindPropertyRelative ("pool");
		poolCollapse = property.FindPropertyRelative ("poolCollapse");
	}
}

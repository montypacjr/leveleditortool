﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LMGTool
{
	[System.Serializable]
	public class MiscObject
	{
		public GameObject prefab;
		public Color color;
		public bool rotation, scale;
		public Vector3 minRotation = new Vector3 (0, 0, 0);
		public Vector3 maxRotation = new Vector3 (0, 0, 0);
		public Vector3 minScale = new Vector3 (1, 1, 1);
		public Vector3 maxScale = new Vector3 (1, 1, 1);
		[SerializeField]
		private bool rotCollapse;
		[SerializeField]
		private bool scaleCollapse;
		[SerializeField]
		private bool doubleOption;
	}
}
